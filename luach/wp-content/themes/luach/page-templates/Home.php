<?php
/**
 * Template Name: Home
 *
 * Description: Twenty Twelve loves the no-sidebar look as much as
 * you do. Use this page template to remove the sidebar from any page.
 *
 * Tip: to remove the sidebar from all posts and pages simply remove
 * any active widgets from the Main Sidebar area, and the sidebar will
 * disappear everywhere.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>

<!DOCTYPE html>
<html class="no-js">


<body>

<!-- Preloader -->
	<div id="preloader">
	    <div id="status"></div>
	</div>




<!--Revolution Slider begin-->
<section class="container-slider">	
	<div id="slider">	
		<ul>
			<?php	$args = array( 'post_type' => 'slider');
					$loop = new WP_Query( $args ); 
					while ( $loop->have_posts() ) : $loop->the_post();
echo'<li data-transition="fade" data-slotamount="7">';
				echo the_content();
		echo'</li>';
				endwhile;
				?>
		

		
		</ul>	      
	</div><!-- end Slider -->   
</section><!--end Revolution Slider-->



<section class="content">
	<div class="container-center">

		<div class="tagline col-row animated" data-animation="fadeInUp">			
			<?php

	$the_query = new WP_Query( 'page_id=123' );
	while ( $the_query->have_posts() ) :
	$the_query->the_post();
    
?>
			<section class="two-third">
				<?php  echo the_content(); ?>
					<!--<div class="one-fourth cf">
						<a class="big-button" href="#">Learn More<span class="btn-arrow"><i class="fa fa-angle-right"></i></span></a>	
					</div><!-- end big-button -->
			</section><!-- end section -->
			<?php endwhile;  
			 // reset post data (important!)
    wp_reset_postdata(); ?>
			
			<div class="news_section cf">
				<div class="newletter_container">
				<div style="width:400px; float:left;position:relative; left:-1px; top:24px; font-size:18px;"><b> SUBSCRIBE TO OUR NEWSLETTERS</b></div>
					<?php echo do_shortcode( '[mc4wp_form]' );?>
				</div>					
			</div><!-- end big-button -->
			
		</div><!-- end tagline -->
		
	</div><!-- end container-center -->
</section><!-- end content -->



<section class="content">
	<div class="container-center">
		<div class="magzine_container">
			<div class="one animated" data-animation="fadeInRight">
					<h3>Magazines</h3>
					
					<div class="col-row">
						<div class="magazine">
						<a href="http://cgcolors.com/dev/luach/wp-content/uploads/2014/10/10-7.pdf"    target= "_blank">
							<img src="http://cgcolors.com/dev/luach/wp-content/uploads/2014/10/p1.png" alt=""/></a>
							<a href="http://cgcolors.com/dev/luach/wp-content/uploads/2014/10/10-7.pdf" target= "_blank">Download</a>
						</div><!--magazine end here-->
						
						<div class="magazine">
							<a href="http://cgcolors.com/dev/luach/wp-content/uploads/2014/10/10-8.pdf" target= "_blank">						
							<img src="http://cgcolors.com/dev/luach/wp-content/uploads/2014/10/p2.png" alt=""/></a>
							<a href="http://cgcolors.com/dev/luach/wp-content/uploads/2014/10/10-8.pdf" target= "_blank">Download</a>
						</div><!--magazine end here-->
						
						<div class="magazine">
							<a href="http://cgcolors.com/dev/luach/wp-content/uploads/2014/10/10-19.pdf" target= "_blank">
							<img src="http://cgcolors.com/dev/luach/wp-content/uploads/2014/10/p3.png" alt=""/></a>
							<a href="http://cgcolors.com/dev/luach/wp-content/uploads/2014/10/10-19.pdf" target= "_blank">Download</a>
						</div><!--magazine end here-->
						
						<div class="magazine">
							<a href="http://cgcolors.com/dev/luach/wp-content/uploads/2014/10/10-20.pdf" target= "_blank">
							<img src="http://cgcolors.com/dev/luach/wp-content/uploads/2014/10/p4.png" alt=""/></a>
							<a href="http://cgcolors.com/dev/luach/wp-content/uploads/2014/10/10-20.pdf" target= "_blank">Download</a>
						</div><!--magazine end here-->
						
						<div class="magazine">
							<a href="http://cgcolors.com/dev/luach/wp-content/uploads/2014/10/10-21.pdf" target= "_blank">
							<img src="http://cgcolors.com/dev/luach/wp-content/uploads/2014/10/p5.png" alt=""/></a>
							<a href="http://cgcolors.com/dev/luach/wp-content/uploads/2014/10/10-21.pdf" target= "_blank">Download</a>
						</div><!--magazine end here-->
						
						<div class="magazine"> 
							<a href="http://cgcolors.com/dev/luach/wp-content/uploads/2014/10/10-22.pdf" target= "_blank">
							<img src="http://cgcolors.com/dev/luach/wp-content/uploads/2014/10/p6.png" alt=""/></a>
							<a href="http://cgcolors.com/dev/luach/wp-content/uploads/2014/10/10-22.pdf"target= "_blank">Download</a>
						</div><!--magazine end here-->
						
						<div class="magazine">
							<a href="http://cgcolors.com/dev/luach/wp-content/uploads/2014/10/10-23.pdf" target= "_blank">
							<img src="http://cgcolors.com/dev/luach/wp-content/uploads/2014/10/p7.png" alt=""/></a>
							<a href="http://cgcolors.com/dev/luach/wp-content/uploads/2014/10/10-23.pdf" target= "_blank">Download</a>
						</div><!--magazine end here--> 
						
						
					</div><!-- end col-row -->			
						
			</div><!-- end one-half --> 
		</div><!--magzine_container end here-->	
	</div>
</section>



<section class="color-box-holder box-4 section-bg-1" id="serv">

	<div class="container-center cf">
<?php	$args = array( 'post_type' => 'Services');
					$loop = new WP_Query( $args ); 
					while ( $loop->have_posts() ) : $loop->the_post();?>	
		<div class="color-box animated" data-animation="fadeInUp">
			<div class="color-box-title tilted">
				<h3><?php echo get_the_title(); ?></h3>
				<h4 class="subtitle"><span><?php the_field('subheading'); ?></span></h4>
			</div><!-- end color-box-title -->
				
				<img src="<?php the_field('service_image'); ?>">

				<?php// the_field('Service_Images'); ?>			
				<?php echo the_content(); ?>
				<!--<a class="button small-btn" href="<?php// echo get_permalink(); ?>">Read More</a>--->
		</div><!-- end box -->	
		<?php endwhile; ?>
		
		</div><!-- end container-center -->
	
</section><!-- end color-box-holder -->

<section class="content section-bg-4">

	<div class="container-center">
	
		<div class="section-title align-center">
			<h2>Features and Content</h2>
			<h3 class="sub-heading">Lorem ipsum dolor sit amet dabipus</h3>
		</div>	
		
		<div class="col-row">	
<?php

	$the_query = new WP_Query( 'page_id=54' );
	while ( $the_query->have_posts() ) :
	$the_query->the_post();
    
?>
			<div class="one animated" data-animation="fadeInLeft">
				
			<?php the_content(); ?>
			</div><!-- end one-half --> 
		<?php	 endwhile;
			 // reset post data (important!)
    wp_reset_postdata();?>
			
		</div><!-- end col-row -->		
		
	</div><!-- end container-center -->

</section><!-- end -->




<section class="content">	
	<!-- end container-center -->
</section><!-- end content -->
	
<section class="content light-section section-bg-1 expose">
	<div class="container-center" >	
<?php

	$the_query = new WP_Query( 'page_id=80' );
	while ( $the_query->have_posts() ) :
	$the_query->the_post();
    
?>		
		<div class="col-row">				
			
			<div class="one-half" >
				<div class="animated"  data-animation="flipInX"><?php echo the_post_thumbnail( array(410,374) );  ?></div>
			</div><!-- end  -->
			
			<div class="one-half animated" data-animation="fadeInRight">
				<div class="section-title">
					<h2 class="heading-extra-big"><?php echo get_the_title(); ?></h2>
					<h3 class="sub-heading"><?php the_field('sub_expos'); ?></h3>
				</div>
				<?php echo the_content(); ?>
				<!---<a class="button medium-btn" href="#">Continue Reading</a>--->
			</div><!-- end  -->
			
		</div><!-- end col-row -->	
	<?php	 endwhile;
			 // reset post data (important!)
    wp_reset_postdata();?>		
	</div><!-- end container-center -->
</section><!-- end content -->	




<section class="content">
	<div class="container-center">
	
		<div class="section-title align-center">
			<h2>What Have Others Said</h2>
			<h3 class="sub-heading">Few Words From Our Satisfied Customers</h3>
		</div>	
		
		<div class="col-row">
				<?php	$args = array( 'post_type' => 'Testimonials');
						$loop = new WP_Query( $args ); 
						while ( $loop->have_posts() ) : $loop->the_post();?>		
			<div class="one-half animated" data-animation="fadeInLeft">	
			
				<div class="testimony cf">
					<div class="testimony-inner">
						<?php the_post_thumbnail('110,110', array('class' => 'portrait')); ?>						
						<blockquote class="testimony-text">
							<?php echo the_content(); ?>									
						</blockquote>	
						
						<div class="testimony-author">
							<h4><?php echo get_the_author(); ?> </h4>
							<span><?php the_field('author_title'); ?></span>						
						</div><!-- end testimony-author -->
					</div><!-- end testimony -->
				</div><!-- end testimonial -->
				
			</div><!-- end one-half -->	
			<?php endwhile; ?>
			
			
		</div><!-- end col-row -->		
		
	</div><!-- end container-center -->
</section><!-- end content -->

<div class="separator-1"></div><!-- end separator -->

<section class="content">
	<div class="container-center">			
		<div class="section-title align-center">
			<h2>Our Top Clients</h2>
			<h3 class="sub-heading">Some of Our Satisfied Clients</h3>
		</div>		
		
		<ul class="col-row clients">
		<?php	$args = array( 'post_type' => 'Clients');
						$loop = new WP_Query( $args ); 
						while ( $loop->have_posts() ) : $loop->the_post();?>
			<li class="one-sixth"><a href="#"><?php echo the_post_thumbnail( array(410,374) );  ?></li>
			<?php endwhile; ?>
			</ul>		
		
	</div><!-- end container-center -->
</section>	
	
<section class="twitter-holder section-bg-1">
	<div class="container-center">	
		<h2>connect with us </h2>				
		<!--<div class="twitter-feed"></div> end twitter-feed -->
			
		<div class="social_icons">		
				<div class="face"><a href="https://twitter.com/luachhatzibur" target="_blank" class="twitter-icon"><i class="fa fa-twitter"></i></a></div><!-- end twitter-icon -->	
			<div class="face"><a href="https://www.facebook.com/LuachHatzibur" target="_blank" class="twitter-icon"><i class="fa fa-facebook"></i></a></div><!-- end twitter-icon -->	
		</div>			
	</div><!-- container-center -->
</section>



<div class="scroll-top"><a href="#"><i class="fa fa-angle-up"></i></a></div>

</body>
<script type="text/javascript" >
$(document).ready(function() {
$(".big-button").click(function() {
    $('html, body').animate({
        scrollTop: $(".expose").offset().top
    }, 2000);
});
$(".service").click(function() {
    $('html, body').animate({
        scrollTop: $("#serv").offset().top
    }, 2000);
});

$(".about").click(function() {
    $('html, body').animate({
        scrollTop: $(".expose").offset().top
    }, 2000);
}); 
$(".jobs").click(function() {
    $('html, body').animate({
        scrollTop: $("#job").offset().top
    }, 2000);
});
$(".cdfooter").click(function() {
    $('html, body').animate({
        scrollTop: $("#footer").offset().top
    }, 2000);
});

});
</script> 
</html>

<?php get_footer(); ?>